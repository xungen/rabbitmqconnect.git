#include <unistd.h>
#include <iostream>
#include "RabbitmqConnect.h"

int main(int argc, char** argv)
{
	int port = 5672;
	string host = "127.0.0.1";

	string user = "admin";
	string passwd = "admin";
	string exchange = "exchange";
	string queuename = "queuename";

	RabbitmqConnect conn;

	if (conn.connect(host, port) && conn.login(user, passwd))
	{
		cout << "连接消息队列[" << host << ":" << port << "][" << user << "]成功" << endl;
		
		if (conn.send(exchange, queuename, "message") < 0)
		{
			cout << "发送消息成功" << endl;
		}
		else
		{
			cout << "发送消息失败[" << conn.getErrorCode() << "][" << conn.getErrorString() << "]" << endl;
		}
	}
	else
	{
		cout << "连接消息队列[" << host << ":" << port << "][" << user << "]失败" << endl;
	}

	while (true)
	{
		RabbitmqConnect conn;

		if (conn.connect(host, port) && conn.login(user, passwd))
		{
			cout << "开始监听消息队列[" << exchange << "][" << queuename << "]" << endl;

			while (true)
			{
				conn.recv(exchange, queuename, [](const char* data, int len){
					string msg(data, data + len);

					cout << "收到消息[" << msg << "]" << endl;
				});
				
				//连接异常需要重连
				if (conn.getErrorCode() == AMQP_STATUS_SOCKET_ERROR) break;
			}

			cout << "监听消息队列[" << exchange << "][" << queuename << "]失败[" << conn.getErrorCode() << "][" << conn.getErrorString() << "]" << endl;
		}
		else
		{
			cout << "连接消息队列[" << host << ":" << port << "][" << user << "]失败" << endl;
		}

		sleep(5);
	}
	
	return 0;
}