target: app

app: RabbitmqConnect.h Example.cpp
	g++ -std=c++11 -lrabbitmq -lssl -lcrypto -lpthread -lutil -lbsd -ldl -lm -o Example Example.cpp

clean:
	@rm Example
